const easyvk = require('easyvk');
const fs = require('fs');
const request = require('request');
const readline = require('readline');
const { USERNAME, PASSWORD, DESTINATION, ID } = require('./config');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const download = (url, filename, callback) => {
  request.head(url, (err, res, body) => {
    request(url).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

function main(count, offset) {
  try {
    easyvk({
      username: USERNAME,
      password: PASSWORD,
    }).then(async (vk) => {
      let albumId = '';
      let photoIds = '';

      let getPosts = await vk
        .call('wall.get', {
          owner_id: ID,
          count: count,
          offset: offset,
        })
        .then(async (getPosts) => {
          photoIds = '';
          albumId = getPosts.items[0].attachments[0].photo.album_id;

          for (item of getPosts.items) {
            try {
              for (img of item.attachments) {
                photoIds += img.photo.id + ',';
              }
            } catch {
              continue;
            }
          }
        });

      let getPhotos = await vk
        .call('photos.get', {
          owner_id: ID,
          album_id: albumId,
          photo_ids: photoIds,
        })
        .then(async (getPhotos) => {
          let urls = [];
          for (photo of getPhotos.items) {
            urls.push(photo.sizes[photo.sizes.length - 1].url);
          }

          for (url of urls) {
            let imgName = url.split('/');
            imgName = imgName[imgName.length - 1];
            let fullDest = `${DESTINATION + '/' + imgName}`;

            download(url, fullDest, () => {
              console.log(`Image ${imgName} was downloaded!`);
            });
          }
        });
    });
  } catch {
    console.log('Something went wrong');
  }
}

let count, offset;

rl.question(
  'Write count of posts (max 100) and offset from a last post (max 1000): ',
  (answer) => {
    try {
      rl.close();
      let answers = answer.split(' ');
      count = answers[0];
      offset = answers[1];

      if (count < 1 || count > 100) {
        throw new Error("'count' have to be in range [1, 100]");
      }

      if (offset < 0 || offset > 1000) {
        throw new Error("'offset' have to be in range [0, 1000]");
      }

      main(count, offset);
    } catch (err) {
      console.log(err);
    }
  }
);

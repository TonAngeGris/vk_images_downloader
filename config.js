const USERNAME = 'your username';
const PASSWORD = 'your password';
const DESTINATION = 'path to needed folder';
// If you want to use group ID, you have to use '-' before that
// For example:
// UserID  : 1234567
// GroupID : -1234567 
const ID = 'user/group id';

module.exports = {
  USERNAME: USERNAME,
  PASSWORD: PASSWORD,
  ID: ID,
  DESTINATION: DESTINATION
};
